<?php

namespace Vocket;

class Call
{
    private $channel;
    private $event;
    private $data;

    /**
     * @param string $channel
     * @param string $event
     * @param array  $data
     */
    public function __construct($channel, $event, array $data)
    {
        $this->channel = $channel;
        $this->event = $event;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function build()
    {
        return json_encode($this->toHash());
    }

    public function send()
    {
        $context = new \ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'VOCKET');
        $socket->connect("tcp://127.0.0.1:6500");

        $socket->send($this->build());
    }

    /**
     * @return array
     */
    public function toHash()
    {
        return array(
            'channel' => $this->channel,
            'event' => $this->event,
            'data' => $this->data
        );
    }

}