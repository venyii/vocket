<?php

namespace Vocket;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Server implements WampServerInterface
{

    protected $subscribedTopics = array();

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        if (!array_key_exists($topic->getId(), $this->subscribedTopics)) {
            $this->subscribedTopics[$topic->getId()] = $topic;
        }
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        echo "Good Bye! ({$conn->resourceId})\n";
    }

    public function onOpen(ConnectionInterface $conn)
    {
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onClose(ConnectionInterface $conn)
    {
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        echo 'err1';
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        echo 'err2';
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo 'err3';
    }

    public function roflcat($entry)
    {
        echo 'entry ' . uniqid() . PHP_EOL;

        $entryData = json_decode($entry, true);
        $channel = $entryData['channel'];
        $event = $entryData['event'];

        echo 'channel: ' . $channel . PHP_EOL;
        echo 'event: ' . $event . PHP_EOL;

        // If the lookup topic object isn't set there is no one to publish to
        if (!array_key_exists($channel, $this->subscribedTopics)) {
            return;
        }

        $topic = $this->subscribedTopics[$channel];

        // re-send the data to all the clients subscribed to that category
        $topic->broadcast($entryData['data']);
    }

}
