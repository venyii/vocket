<?php

require dirname(__DIR__) . '/vendor/autoload.php';

$channel = isset($argv[1]) ? $argv[1] : 'checked-players';

$data = array(
    'count' => 3,
    'players' => array(
        3 => array(
            'id' => "3",
            'avatar' => "/static/upload/avatar/dummy.png",
            'firstname' => "John",
            'lastname' => "Doe",
            'uid' => "john.doe"
        ),
        5 => array(
            'id' => "5",
            'avatar' => "/static/upload/avatar/dummy.png",
            'firstname' => "John",
            'lastname' => "Doe",
            'uid' => "john.doe"
        ),
        9 => array(
            'id' => "9",
            'avatar' => "/static/upload/avatar/dummy.png",
            'firstname' => "John",
            'lastname' => "Doe",
            'uid' => "john.doe"
        )
    ));

$call = new \Vocket\Call($channel, 'random', $data);
$call->send();
