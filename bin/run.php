<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\Wamp\WampServer;
use React\EventLoop\Factory;

use Vocket\Server;

$loop = Factory::create();
$pusher = new Server();

// Listen for the web server to make a ZeroMQ push
// Binding to 127.0.0.1 means the only client that can connect is itself
$context = new React\ZMQ\Context($loop);
$pull = $context->getSocket(ZMQ::SOCKET_PULL);
$pull->bind('tcp://127.0.0.1:6500');
$pull->on('message', array($pusher, 'roflcat'));

// Set up our WebSocket server for clients wanting real-time updates
// Binding to 0.0.0.0 means remotes can connect
$webSock = new React\Socket\Server($loop);
$webSock->listen(6100, '0.0.0.0');
$webServer = new IoServer(
    new WsServer(
        new WampServer(
            $pusher
        )
    ),
    $webSock
);

$loop->run();
